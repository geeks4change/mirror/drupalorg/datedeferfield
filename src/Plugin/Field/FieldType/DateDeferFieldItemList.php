<?php

namespace Drupal\datedeferfield\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemList;

/**
 * Class DateDeferFieldItemList
 *
 * For now, extend nothing. If we want relative default dates, look into
 * @see \Drupal\datetime\Plugin\Field\FieldType\DateTimeFieldItemList
 */
class DateDeferFieldItemList extends FieldItemList {}
