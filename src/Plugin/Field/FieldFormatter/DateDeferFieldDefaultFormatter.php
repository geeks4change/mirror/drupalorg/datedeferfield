<?php

namespace Drupal\datedeferfield\Plugin\Field\FieldFormatter;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\datedeferfield\Plugin\Field\FieldType\DateDeferFieldItem;

/**
 * Plugin implementation of the 'datedeferfield_default' formatter.
 *
 * @FieldFormatter(
 *   id = "datedeferfield_default",
 *   label = @Translation("Complete info"),
 *   field_types = {
 *     "datedeferfield"
 *   }
 * )
 */
class DateDeferFieldDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $now = DateTimePlus::createFromTimestamp(\Drupal::time()->getRequestTime());
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta]['#theme'] = 'datedeferfield_formatter';
      $elements[$delta]['#labels']['deferred'] = $this->t('Deferred');
      foreach (DateDeferFieldItem::components() as $key => $label) {
        /** @var \Drupal\Core\Datetime\DrupalDateTime $date */
        $date = $item->{"date_$key"};
        $elements[$delta]['#labels'][$key] = $label;
        $elements[$delta]['#dates'][$key] = $date;
        // The diff is positive if that date has passed.
        $elements[$delta]['#diffs'][$key] = !isset($date) ? NULL :
          $date->diff($now)->format('%r%a');
        if (!empty($item->_attributes)) {
          $elements[$delta][$key]['date']['#attributes'] += $item->_attributes;
          // Unset field item attributes since they have been included in the
          // formatter output and should not be rendered in the field template.
          unset($item->_attributes);
        }
      }
    }
    // As we format relative dates, this expires midnight, neglecting timezones.
    // @todo Improve.
    $maxAge = (new DateTimePlus('tomorrow'))->getTimestamp() - $now->getTimestamp();
    $elements['#cache']['max-age'] = $maxAge;

    return $elements;
  }

}
