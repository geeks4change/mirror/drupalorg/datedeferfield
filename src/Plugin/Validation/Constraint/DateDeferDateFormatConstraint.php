<?php

namespace Drupal\datedeferfield\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Validation constraint for DateTime items to ensure the format is correct.
 *
 * @Constraint(
 *   id = "DateDeferDateFormat",
 *   label = @Translation("Datetime format valid for datetime type.", context = "Validation"),
 * )
 */
class DateDeferDateFormatConstraint extends Constraint {

  /**
   * Message for when the value isn't a string.
   *
   * @var string
   */
  public $badType = "The @component date value must be a string.";

  /**
   * Message for when the value isn't in the proper format.
   *
   * @var string
   */
  public $badFormat = "The @component date value '@value' is invalid for the format '@format'";

  /**
   * Message for when the value did not parse properly.
   *
   * @var string
   */
  public $badValue = "The @component date value '@value' did not parse properly for the format '@format'";

}
