<?php

namespace Drupal\datedeferfield\Plugin\Validation\Constraint;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\datedeferfield\Plugin\Field\FieldType\DateDeferFieldItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Constraint validator for DateDefer items to ensure the format is correct.
 */
class DateDeferDateFormatConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($item, Constraint $constraint) {
    $format = DateTimeItemInterface::DATE_STORAGE_FORMAT;
    /* @var $item \Drupal\datedeferfield\Plugin\Field\FieldType\DateDeferFieldItem */
    if (isset($item)) {
      foreach (DateDeferFieldItem::components() as $key => $label) {
        $value = $item->getValue()["value_$key"];
        $args = [
          '@component' => $label,
          '@value' => $value,
          '@format' => $format,
        ];
        if (isset($value)) {
          if (!is_string($value)) {
            $this->context->addViolation($constraint->badType, $args);
          }
          else {
            $date = NULL;
            try {
              $date = DateTimePlus::createFromFormat($format, $value, new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
            }
            catch (\InvalidArgumentException $e) {
              $this->context->addViolation($constraint->badFormat, $args);
              return;
            }
            catch (\UnexpectedValueException $e) {
              $this->context->addViolation($constraint->badValue, $args);
              return;
            }
            if ($date === NULL || $date->hasErrors()) {
              $this->context->addViolation($constraint->badFormat, $args);
            }
          }
        }
      }
    }
  }

}
